﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelmanagerScript : MonoBehaviour {

    public int deathCount;

	// Use this for initialization
	void Start () {

        deathCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
        if (deathCount >= 3)
        {
            SceneManager.LoadScene(0);

        }
	}
}
