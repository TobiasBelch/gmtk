﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

    public ElementalParameter parameters;
    GameObject player;
    public GameObject shotPoint;
    string eleType;
    Rigidbody2D rb;
    public ParticleSystemRenderer trailParticle;
    public ParticleSystemRenderer shotParticle;
    Material particleMat;
    SpriteRenderer shipSprite;
    SpriteRenderer shieldSprite;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody2D>();

        eleType = parameters.elementType;
        particleMat = parameters.particleMat;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        LookAtPlayer();
        Move();
	}

    void LookAtPlayer()
    {
        Vector3 diff = player.transform.position - transform.position;
        diff.z = 0;

        Quaternion lookRotation = Quaternion.LookRotation(transform.forward, diff.normalized);

        transform.rotation = lookRotation;
    }

    void Move()
    {
        Vector2 moveVector;
        moveVector = player.transform.position - this.transform.position;
        rb.AddForce(moveVector.normalized * parameters.moveSpeed);
    }
}
