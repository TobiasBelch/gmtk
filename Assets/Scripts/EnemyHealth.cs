﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public float startingHealth = 100f;
    public float currentHealth;

    public GameObject shieldDrop;

    EnemyBehaviour enemyBehaviour;

    bool isDead;



	// Use this for initialization
	void Start () {
        enemyBehaviour = GetComponent <EnemyBehaviour> ();
        currentHealth = startingHealth;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TakeDamage (float amount, ElementalParameter shotParam)
    {
        string damageType = shotParam.elementType;
        string enemyType = enemyBehaviour.parameters.elementType;
        if (IsVulnerable(damageType, enemyType))
        {
            if (currentHealth <= 0)
            {
                Death();
            }
            else currentHealth -= amount;
            Debug.Log(amount + " damage taken.");
            if (currentHealth <= 0)
            {
                //enemyBehaviour.shieldSprite;
            }
        }
        else
        {
            Debug.Log("Not Vulnerable to " + shotParam.elementType);
        }
    }

    bool IsVulnerable(string damageType, string enemyType)
    {
        bool vulnerable = false;

        switch (damageType)
        {
            case "Thermal":
                if (enemyType == "Corrosive")
                {
                    vulnerable = true;
                }
                break;
            case "Corrosive":
                if (enemyType == "Cryo")
                {
                    vulnerable = true;
                }
                break;
            case "Cryo":
                if (enemyType == "Thermal")
                {
                    vulnerable = true;
                }
                break;
        }

        return vulnerable;
    }

    void Death ()
    {
        isDead = true;
        Instantiate(shieldDrop, transform.position, transform.rotation);

        LevelmanagerScript manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<LevelmanagerScript>();

        manager.deathCount++;
        Destroy(this.gameObject);
    }
}
