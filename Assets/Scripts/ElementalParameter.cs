﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ElementalParameter : ScriptableObject
{
    public string elementType;
    public Sprite elementalParticle;
    public Material particleMat;
    public Sprite shipSprite;
    public Sprite shieldSprite;
    public float shootRange;
    public float shotDamage;
    public Sprite pickupSprite;
    public Sprite playerSprite;
    public Sprite playerShieldSprite;
    public float moveSpeed;
}
