﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTracking : MonoBehaviour {

    GameObject player;
    PlayerController playerScript;    
    Rigidbody2D rb;

    Vector3 mousePoint;
    Vector3 startOffset;

    public float maxCamDistance;
    public float maxCamSpeed;
    public float camSpeed;
    public float speedMod;

    // Use this for initialization
    void Start ()
    {
        startOffset = transform.position;
        player = GameObject.FindGameObjectWithTag("Player");
        playerScript = player.GetComponent<PlayerController>();
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        mousePoint = playerScript.rayPoint;
        MoveCam();

	}

    void MoveCam()
    {
        Vector3 moveAmount;
        
        moveAmount = (mousePoint - player.transform.position)/2;
        Vector3 targetPos = player.transform.position + moveAmount + startOffset;

        camSpeed = ((targetPos - transform.position).magnitude * speedMod);

        if (camSpeed > maxCamSpeed)
        {
            camSpeed = maxCamSpeed;
        }

        if ((targetPos - player.transform.position).magnitude > maxCamDistance)
        {
            float fraction = (maxCamDistance / ((targetPos - player.transform.position).magnitude));
            moveAmount *= fraction;
            targetPos = player.transform.position + moveAmount + startOffset;

            float step = camSpeed * Time.deltaTime * 2;
            transform.position = Vector3.MoveTowards(transform.position, targetPos, step);
        }
        else
        {
            float step = camSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetPos, step);
        }
    }
}
