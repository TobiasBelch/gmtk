﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldScript : MonoBehaviour {

    ParticleCollisionEvent currentCollisions;
    public GameObject player;
    public GameObject particle;

    Vector2 movement;
    int floorMask;
    float camRayLength = 100f;
    public float shotReboundMod;

    Vector3 mousePos;

    public ElementalParameter element;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        floorMask = LayerMask.GetMask("Floor");

        floorMask = 1 << 8;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
    
	}

    public void ReflectParticle(Vector3 point, ElementalParameter param, Vector3 shotVelocity)
    {
        Vector3 direction = player.transform.up;
        GameObject reflectedParticle;
        reflectedParticle = Instantiate(particle, point, player.transform.rotation);
        if (element != null)
        {
            reflectedParticle.GetComponent<PlayerParticleScript>().shotParams = element;
        }
        else
        {
            reflectedParticle.GetComponent<PlayerParticleScript>().shotParams = param;
        }
        Vector2 shotDirection = mousePos - player.transform.position;
        reflectedParticle.GetComponent<Rigidbody2D>().velocity = shotVelocity.magnitude * shotDirection.normalized * shotReboundMod;
    }

    public void Tracking()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            mousePos = floorHit.point;
            Vector2 playerToMouse = mousePos - player.transform.position;

            Quaternion newRotation = Quaternion.LookRotation(transform.forward, playerToMouse);

            transform.rotation = newRotation;
        }
    }
}
