﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rb;

    public float speed;

    int floorMask;
    float camRayLength = 100f;
    public Vector3 rayPoint;
    public ShieldScript shield;

    void Start()
    {
        shield = GetComponentInChildren<ShieldScript>();

        rb = GetComponent<Rigidbody2D>();

        floorMask = 1 << 8;
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");       

        Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0.0f);
        rb.velocity = movement * speed;
        Turning();
        shield.Tracking();
    }


    void Turning()
    {
        if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
        {
            Vector3 inputDirection;
            inputDirection = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0.0f);

            inputDirection *= 10;

            Vector3 playerToDirection = this.transform.position + inputDirection;

            Quaternion newRotation = Quaternion.LookRotation(transform.forward, playerToDirection);

            transform.rotation = newRotation;
        }
    }

}
