﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollisionScript : MonoBehaviour {

    ParticleSystem shotSystem;
    public List<ParticleCollisionEvent> collisionEvents;
    EnemyBehaviour parentEnemy;
    ElementalParameter parameters;

    // Use this for initialization
    void Start () {
        shotSystem = GetComponent<ParticleSystem>();
        parentEnemy = GetComponentInParent<EnemyBehaviour>();
        parameters = parentEnemy.parameters;
        collisionEvents = new List<ParticleCollisionEvent>();

        var main = shotSystem.main;
        main.startRotation = parentEnemy.gameObject.transform.localRotation.z;
    }
	
	// Update is called once per frame
	void Update ()
    {
        var main = shotSystem.main;
        main.startRotation = parentEnemy.gameObject.transform.localRotation.z;
    }

    private void OnParticleCollision(GameObject collisionObj)
    {
        int numCollisionEvents = shotSystem.GetCollisionEvents(collisionObj, collisionEvents);

        foreach (ParticleCollisionEvent col in collisionEvents)
        {
                if (col.colliderComponent.tag == "Shield")
                {
                    collisionObj.GetComponentInChildren<ShieldScript>().ReflectParticle(col.intersection, parameters, col.velocity);
                }
                else if (col.colliderComponent.tag == "Player")
                {
                    collisionObj.GetComponent<PlayerHealth>().TakeDamage((int)parameters.shotDamage);
                }
        }
    }
}
