﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementalPickup : MonoBehaviour {

    public ElementalParameter elementType;
    public float spinSpeed;
    Rigidbody2D rb;
    SpriteRenderer spriteR;



	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteR = GetComponent<SpriteRenderer>();
        spriteR.sprite = elementType.pickupSprite;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Vector3 angle = transform.rotation.eulerAngles;

        angle.z += spinSpeed;

        Quaternion newAngle = Quaternion.Euler(angle);

        transform.rotation = newAngle;

	}

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player") 
        {
            col.gameObject.GetComponentInChildren<ShieldScript>().element = elementType;
            col.gameObject.GetComponent<SpriteRenderer>().sprite = elementType.playerSprite;
            col.gameObject.GetComponent<PlayerController>().shield.GetComponentInChildren<SpriteRenderer>().sprite = elementType.playerShieldSprite;
            Destroy(this.gameObject);
        }
        else if (col.gameObject.tag == "Shield")
        {
            col.gameObject.GetComponent<ShieldScript>().element = elementType;
            col.gameObject.GetComponentInChildren<SpriteRenderer>().sprite = elementType.playerShieldSprite;
            col.gameObject.GetComponent<ShieldScript>().player.GetComponent<SpriteRenderer>().sprite = elementType.playerSprite;
            Destroy(this.gameObject);
        }
    }
}
