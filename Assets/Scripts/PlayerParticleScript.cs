﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticleScript : MonoBehaviour {

    public ElementalParameter shotParams;
    SpriteRenderer spriteR;
    Sprite newSprite;
    Rigidbody rb;

	// Use this for initialization
	void Start ()
    {
        spriteR = GetComponent<SpriteRenderer>();
        string sName;
        sName = "Sprites/Enemy/Particles/" + shotParams.elementalParticle.name;
        newSprite = Resources.Load<Sprite>(sName);
        spriteR.sprite = newSprite;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D col)
    {
        int colLayer = col.gameObject.layer;

        switch (colLayer)
        {
            case 9:
                Debug.Log("Enemy Hit");
                col.gameObject.GetComponent<EnemyHealth>().TakeDamage(shotParams.shotDamage, shotParams);
                Destroy(this.gameObject);
                break;
            case 8:
                Debug.Log("Environment Hit");
                Destroy(this.gameObject);
                break;
        }
    }
}
